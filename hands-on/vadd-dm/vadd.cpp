#include <vector>
#include <cstdlib>
#include <cstdio>
#include <time.h>

/** How to compile and execute this code: (Remeber to change the target to 'alveo' if using FPGAs)

  clang++ -fopenmp -fopenmp-targets=x86_64-pc-linux-gnu -fno-openmp-new-driver vadd.cpp -o vadd
  mpirun -np 3 ./vadd

*/

#define ARRAY_SIZE 4096 // Size of buffers
#define BLOCK_SIZE 256 // Size of each block

void vadd(int *in1, int *in2, int *out, size_t N) {
 for (size_t i = 0; i < N; i++)
   out[i] = in1[i] + in2[i];
}

/** Implement your OpenMP Version here. Use the 'vadd' function above to do
    a summation of the blocks for your tasks.
    int *in1: Input number 1
    int *in2: Input number 2
    int *in3: Input number 3
    int *in4: Input number 4 
    int *out: Output array
    int N: Size of all arrays
    int BS: Size of each block
*/
void vector_reduction(int *in1, int *in2, int *in3, int *in4, int *out, int N, int BS) {

  // TODO: Implement your version here. Tip: Start from your first version
  // Don't forget to use 'target enter data' and 'target exit data'

}

// Please, don't touch the main. It is prepared to give some inputs
// and check some outputs for you code. Feel free to inspect and see what the 
// function does
int main(int argc, char **argv) {
  srand(time(NULL));
  int A[ARRAY_SIZE], B[ARRAY_SIZE], C[ARRAY_SIZE], D[ARRAY_SIZE], out[ARRAY_SIZE];

  // Initialize vectors
  for (int i = 0; i < ARRAY_SIZE; i++) {
    A[i] = rand() % 1000;
    B[i] = rand() % 1000;
    C[i] = rand() % 1000;
    D[i] = rand() % 1000;
    out[i] = 0;
  }
  
  // Call the blocked OpenMP Target version
  vector_reduction(A, B, C, D, out, ARRAY_SIZE, BLOCK_SIZE);

  // Result check
  bool match = true;
  for (int i = 0; i < ARRAY_SIZE; i++) {
    if (A[i] + B[i] + C[i] + D[i] != out[i]) {
      match = false;
      break;
    }
  }

  printf("Offloaded computation %s\n", (match) ? "succeeded" : "failed");

  return (match) ? EXIT_SUCCESS : EXIT_FAILURE;
}
