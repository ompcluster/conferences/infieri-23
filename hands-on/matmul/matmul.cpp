#include <cstdio>
#include <cstdlib>
#include <vector>

#include "matmul.h"

/** How to compile and execute this code: (Remeber to change the target to 'alveo' if using FPGAs)

  clang++ -fopenmp -fopenmp-targets=x86_64-pc-linux-gnu -fno-openmp-new-driver -I../../common matmul.cpp -o matmul
  mpirun -np 3 ./matmul

*/

void matmul(BlockMatrix &A, BlockMatrix &B, BlockMatrix &C, size_t N,
            size_t BS) {
  // To be implemented
}

void BlockSerialMatmul(BlockMatrix &A, BlockMatrix &B, BlockMatrix &C, size_t N,
                       size_t BS) {
  int *BA, *BB, *BC;
  for (int i = 0; i < N / BS; ++i)
    for (int j = 0; j < N / BS; ++j)
      for (int k = 0; k < N / BS; ++k) {
        BC = C.GetBlock(i, j);
        BA = A.GetBlock(i, k);
        BB = B.GetBlock(k, j);

        // Multiply blocks
        for (int ii = 0; ii < BS; ++ii)
          for (int jj = 0; jj < BS; ++jj)
            for (int kk = 0; kk < BS; ++kk)
              BC[ii + jj * BS] += BA[ii + kk * BS] * BB[kk + jj * BS];
      }
}

void SerialMatmul(const int *A, const int *B, int *C, size_t N) {
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
      for (int k = 0; k < N; k++)
        C[i * N + j] += A[i * N + k] * B[k * N + j];
}

int main(int argc, char *argv[]) {
  double t;
  size_t N = 1024;
  size_t BS = 256;

  std::vector<int> a(N * N);
  std::vector<int> b(N * N);
  std::vector<int> c(N * N, 0.0);

  auto BlockedA = BlockMatrix(BS, BS, N, N);
  auto BlockedB = BlockMatrix(BS, BS, N, N);
  auto BlockedC = BlockMatrix(BS, BS, N, N);

  std::srand((unsigned int)std::time(nullptr));
  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j) {
      a[i * N + j] = rand() % 10;
      b[i * N + j] = rand() % 10;
    }
  }

  BlockedA.Initialize(a);
  BlockedB.Initialize(b);
  BlockedC.Initialize(c);

  fprintf(stdout, "<< Matrix Multiplication >>\n");

  t = omp_get_wtime();
  SerialMatmul(a.data(), b.data(), c.data(), N);
  t = omp_get_wtime() - t;
  fprintf(stdout, "Local MatMul Computation done in %0.6lfs\n", t);

  // Implement this function using OMPC
  t = omp_get_wtime();
  matmul(BlockedA, BlockedB, BlockedC, N, BS);
  t = omp_get_wtime() - t;
  fprintf(stdout, "Offloaded BlockMatMul Computation done in %0.6lfs\n", t);

  int failed = BlockedC.Compare(c);

  printf("Offloaded computation %s\n", (failed == 0) ? "succeeded" : "failed");

  return (failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
