#include <vector>
#include <cstdlib>
#include <cstdio>
#include <time.h>

/** How to compile and execute this code: (Remeber to change the target to 'alveo' if using FPGAs)
  unset XILINX_XRT
  clang++ -fopenmp -fopenmp-targets=x86_64-pc-linux-gnu -fno-openmp-new-driver vadd.cpp -o vadd
  mpirun -np 3 ./vadd

*/

#define ARRAY_SIZE 4096 // Size of buffers
#define BLOCK_SIZE 256 // Size of each block

void vadd(int *in1, int *in2, int *out, size_t N) {
 for (size_t i = 0; i < N; i++)
   out[i] = in1[i] + in2[i];
}

void blocked_vadd(int *in1, int *in2, int *out, int N, int BS) {
 for(int i = 0; i < N / BS; i++) {
   int *A  = &in1[BS * i], *B  = &in2[BS * i], *C  = &out[BS * i];
   #pragma omp target nowait          \
           map(to: A[:BS], B[:BS])    \
           map(from: C[:BS])          \
           depend(in: A[0], B[0])     \
           depend(out: C[0])
   vadd(A, B, C, BS);
 }
 #pragma omp taskwait
}

int main(int argc, char **argv) {
  srand(time(NULL));
  int A[ARRAY_SIZE], B[ARRAY_SIZE], out[ARRAY_SIZE];

  // Initialize vectors
  for (int i = 0; i < ARRAY_SIZE; i++) {
    A[i] = rand() % 1000;
    B[i] = rand() % 1000;
    out[i] = 0;
  }
  
  // Call the blocked OpenMP Target version
  blocked_vadd(A, B, out, ARRAY_SIZE, BLOCK_SIZE);

  // Result check
  bool match = true;
  for (int i = 0; i < ARRAY_SIZE; i++) {
    if (A[i] + B[i] != out[i]) {
      match = false;
      break;
    }
  }

  printf("Offloaded computation %s\n", (match) ? "succeeded" : "failed");

  return (match) ? EXIT_SUCCESS : EXIT_FAILURE;
}
