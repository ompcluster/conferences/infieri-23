#include <cstdio>
#include <unistd.h>
#include <omp.h>

/** How to compile and execute this code: 
  unset XILINX_XRT
  clang++ -fopenmp -fopenmp-targets=x86_64-pc-linux-gnu -fno-openmp-new-driver hello-world.cpp -o hello-world
  mpirun -np 3 ./hello-world

*/

int main(int argc, char **argv) {
  int pid = getpid();
  printf("[head] pid = %d @ %p\n", pid, &pid);	

  #pragma omp target depend(in: pid) nowait
  {
    printf("[worker] pid = %d (head pid %d @ %p)\n", getpid(), pid, &pid);
  }
  #pragma omp taskwait

  return 0;
}
