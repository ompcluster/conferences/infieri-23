OMPC Tutorial (INFIERI'23)
================================================================================

This repository contains the necessary material to follow the hands-on "Starting to use the OpenMP Cluster programming model" presented at INFIERI'23 held in São Paulo - SP, Brazil.


> **Note**
> The presentation is available [here](slides.pdf).


Compiling and Running programs
--------------------------------------------------------------------------------

The OMPC runtime was built on top of the `libomptarget` OpenMP interface in the LLVM compiler. Developers need to use our `clang` versions to compile their applications. In the servers used on this hands-on, our `clang` is set and ready to use.


To compile an OMPC application:
```console
$ clang++ -fopenmp -fopenmp-targets=x86_64-pc-linux-gnu -fno-openmp-new-driver program.cpp -o program
```

To compile an OMPC application for FPGAs:
```console
$ clang++ -fopenmp -fopenmp-targets=alveo -fno-openmp-new-driver program.cpp -o program
```

After compiling the application, the program will be executed using `mpirun` with the number of process to run the application. Other MPI flals would apply (*e.g.:* `--hostfile`)

To execute a compiled applicat, wether using FPGA or not: 
```console
$ mpirun -np 3 ./program
```


Additional information
--------------------------------------------------------------------------------

The source code of OMPC runtime is available in the [Gitlab][gitlab]. If you find any non-expected behavior or issues, please warn us by creating an [issue][issues] in Gitlab.
The OMPC documentation can be found in our [page][rtdocs].


<!----------------------------------- LINKS ----------------------------------->
[gitlab]: https://gitlab.com/ompcluster/llvm-project
[issues]: https://gitlab.com/ompcluster/llvm-project/-/issues
[rtdocs]: https://ompcluster.readthedocs.io

