#include <vector>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <limits>
#include <memory>
#include <vector>

#include <omp.h>

class BlockMatrix {
private:
  const int rowsPerBlock;
  const int colsPerBlock;
  const int nRows;
  const int nCols;
  const int nBlocksPerRow;
  const int nBlocksPerCol;
  std::vector<std::vector<std::unique_ptr<int[]>>> Blocks;

public:
  BlockMatrix(const int _rowsPerBlock, const int _colsPerBlock,
              const int _nRows, const int _nCols)
      : rowsPerBlock(_rowsPerBlock), colsPerBlock(_colsPerBlock), nRows(_nRows),
        nCols(_nCols), nBlocksPerRow(_nRows / _rowsPerBlock),
        nBlocksPerCol(_nCols / _colsPerBlock), Blocks(nBlocksPerCol) {
    for (int i = 0; i < nBlocksPerCol; i++) {
      for (int j = 0; j < nBlocksPerRow; j++) {
        Blocks[i].emplace_back(new int[_rowsPerBlock * _colsPerBlock]);
      }
    }
  };

  // Initialize the BlockMatrix from 2D arrays
  void Initialize(const std::vector<int> &matrix) {
    for (int i = 0; i < nBlocksPerCol; i++)
      for (int j = 0; j < nBlocksPerRow; j++) {
        int *CurrBlock = GetBlock(i, j);
        for (int ii = 0; ii < colsPerBlock; ++ii)
          for (int jj = 0; jj < rowsPerBlock; ++jj) {
            int curri = i * colsPerBlock + ii;
            int currj = j * rowsPerBlock + jj;
            CurrBlock[ii + jj * colsPerBlock] = matrix[curri + currj * nCols];
          }
      }
  }

  int Compare(const std::vector<int> &matrix) const {
    int fail = 0;
    for (int i = 0; i < nBlocksPerCol; i++)
      for (int j = 0; j < nBlocksPerRow; j++) {
        int *CurrBlock = GetBlock(i, j);
        for (int ii = 0; ii < colsPerBlock; ++ii)
          for (int jj = 0; jj < rowsPerBlock; ++jj) {
            int curri = i * colsPerBlock + ii;
            int currj = j * rowsPerBlock + jj;

            int m_value = matrix[curri + currj * nCols];
            int bm_value = CurrBlock[ii + jj * colsPerBlock];
            if (std::fabs(bm_value - m_value) >
                std::numeric_limits<float>::epsilon()) {
              fail++;
            }
          }
      }

    // Print results
    printf("Non-Matching Block Outputs: %d\n", fail);
    return fail;
  }

  int *GetBlock(int i, int j) const {
    assert(i < nBlocksPerCol && j < nBlocksPerRow && "Accessing outside block");
    return Blocks[i][j].get();
  }

  // Print BlockMatrix
  void Print() {
    for (int i = 0; i < nBlocksPerCol; i++)
      for (int j = 0; j < nBlocksPerRow; j++) {
        int *CurrBlock = GetBlock(i, j);
        printf("Block (%d, %d)\n", i, j);
        for (int ii = 0; ii < colsPerBlock; ++ii) {
          for (int jj = 0; jj < rowsPerBlock; ++jj) {
            printf(" %5d", CurrBlock[ii * colsPerBlock + jj]);
          }
          printf("\n");
        }
        printf("\n");
      }
  }
};

