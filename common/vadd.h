#include <stdlib.h>

/// Generate random array
auto gen_array(int N) {

  return std::vector<int>(N, 0);
}

/// Initialize vector with `N` elements, all initialized with increasing values.
auto create_iota_vector(size_t N) {
  std::vector<int> numbers(N);
  for (size_t i = 0; i < N; i++)
    numbers[i] = i;
  return numbers;
}

/// Check if the results match
bool vadd_check(const std::vector<int> &output) {
  bool match = true;
  for (size_t i = 0; i < output.size(); i++)
    match = match && output[i] == 4 * i;
  return match;
}